# reference-static-analyzer

To do available expressions analysis:

    1- Create the (under analysis)method in a file, using the tool language. You can see some sample methods in the "examples" directory.

    2- Set "--AEA [path to the file]" as the argument of the main method in reference-static-analyzer/src/main/scala/toolc/Main.scala.

    3- Run the "main". First the CFG is printed and then the results of the analysis.

To do interval analysis:

    1- Create the (under analysis)method in a file, using the tool language. You can see some sample methods in the "examples" directory.

    2- Set "--IA [path to the file]" as the argument of the main method in reference-static-analyzer/src/main/scala/toolc/Main.scala.

    3- Run the "main". First the CFG is printed and then the results of the analysis.
    
    notes: 
    
    1- Currently we can only handle simple LessThan statements as conditionals.
    
    2- We do not support Division. 

To import and run the project in the IntelliJ IDE:
    
    1- Clone the project.
    2- Import it in Intellij IDE (Import as an sbt project). While importing the project, deselect "Main" as a Module. Or alternatively in "Project Structure ..." remove "Main" as a module.
    3- Build the project using the "Build Project" option in the Build Menu (compiles with scalar 2.11.8)
    4- Set the arguments for the Main menu as mentioned above
    5- In the configuration menu for  Main, for the field "use classpath of module", select "reference-static-analyser"
    5- Run Main
    
 To run the project using commandline (compiles with compiles with scalar 2.11.8): 
    
    1- Clone the project
    2- Open a terminal in the project path.
	3- Run "sbt"
    3- Run "compile"
	4- Run "run --IA [Path to file]"
    