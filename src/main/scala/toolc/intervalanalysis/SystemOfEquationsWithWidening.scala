package toolc.intervalanalysis

import toolc.ast.Trees
import toolc.ast.Trees.Identifier
import toolc.cfg.CFG
import toolc.intervalanalysis.IntervalAnalysis.computeTransferFunctionForBlock

import scala.collection.immutable.SortedMap

case class TransferFunction(id: String, deltaFunc: DeltaFunc) {}

trait SystemOfEquationsWithWidening {

  def solveSystemOfEquationsUsingWidening(cfg: CFG): SortedMap[Int, DeltaFunc] = {

    var latestSolution: SortedMap[Int, DeltaFunc] = SortedMap.empty
    var currentSolution: SortedMap[Int, DeltaFunc] = SortedMap.empty
    val setOfVars = cfg.setOfBlocks.map(x => Trees.allVariablesOf(x.tree)).toSet.reduce((a1, a2) => a1.union(a2))

    //computes the set of Ks used in widening
    val programConstants = cfg.getConstants.map(i => Integer(i.value)).toSet[IntervalDomain]
    val setOfKs: Set[IntervalDomain] = programConstants.union(Set(PositiveInfinity, NegativeInfinity))

    def updateSolutions(cond: => Boolean, block: => Unit): Unit = {
      block
      if (cond) {
        latestSolution = currentSolution
        currentSolution = SortedMap.empty
        updateSolutions(cond, block)
      }
    }

    def getLatestAvailableSolution(id: Int): DeltaFunc = {
      if (currentSolution.contains(id))
        currentSolution(id)
      else
        latestSolution(id)
    }

    def computeAndUpdateSolutions(): Unit = ???

    //initializes the solutions for each variable in each block with the Bottom Element and  also sorting the generated map
    latestSolution = scala.collection.immutable.SortedMap(cfg.setOfBlocks.map(node => (node.id.toInt, DeltaFunc(setOfVars.map(v => (Identifier(v), Bottom)).toMap))).sortBy(_._1): _*)

    updateSolutions(!latestSolution.equals(currentSolution), computeAndUpdateSolutions())

    currentSolution
  }
}
