package toolc.intervalanalysis

import toolc.ast.Trees
import toolc.ast.Trees._
import toolc.cfg.CFGNode

trait DeltaFuncDomain

case object Bottom extends DeltaFuncDomain {
  override def toString: String = {
    //creates the symbol for the Bottom element
    9524.toChar.toString
  }
}

//the domain for lower and upper bound of an interval
trait IntervalDomain

//a lower bound or upper bound of an interval can be an integer, positive or negative infinity
case class Integer(i: Int) extends IntervalDomain {
  override def toString: String = i.toString
}

case object PositiveInfinity extends IntervalDomain {
  override def toString: String = "+oo"
}

case object NegativeInfinity extends IntervalDomain {
  override def toString: String = "-oo"
}

//interval => [lower bound, upper bound]
case class Interval(lowerBound: IntervalDomain, upperBound: IntervalDomain) extends DeltaFuncDomain {
  override def toString: String = "[" + lowerBound + ", " + upperBound + "]"
}

//the delta function that maps each variable to an interval or the bottom element
case class DeltaFunc(deltaFuncMap: Map[Identifier, DeltaFuncDomain]) {
  override def toString: String = deltaFuncMap.map { case (id, d) => "\t" + id + ": " + d }.mkString("\n")
}

object IntervalAnalysis {

  def computeTransferFunctionForBlock(cfgNode: CFGNode, deltaFunc: DeltaFunc): DeltaFunc = ???

  //interval evaluation function
  def evaluationFunction(expr: ExprTree, deltaFunc: DeltaFunc): DeltaFuncDomain = {

    def computeIntervalForArithmeticExpr(expr: ExprTree): DeltaFuncDomain = {

      expr match {

        case Plus(x, y) =>
          val evalX = evaluationFunction(x, deltaFunc)
          val evalY = evaluationFunction(y, deltaFunc)
          (evalX, evalY) match {
            case (Bottom, _) | (_, Bottom) => Bottom
            case (Interval(l1, h1), Interval(l2, h2)) =>
              Interval(evaluatePlus(l1, l2), evaluatePlus(h1, h2))
          }

        case Minus(x, y) =>
          val evalX = evaluationFunction(x, deltaFunc)
          val evalY = evaluationFunction(y, deltaFunc)
          (evalX, evalY) match {
            case (Bottom, _) | (_, Bottom) => Bottom
            case (Interval(l1, h1), Interval(l2, h2)) =>
              Interval(evaluateMinus(l1, h2), evaluateMinus(h1, l2))
          }

        case Times(x, y) =>
          val evalX = evaluationFunction(x, deltaFunc)
          val evalY = evaluationFunction(y, deltaFunc)
          (evalX, evalY) match {
            case (Bottom, _) | (_, Bottom) => Bottom
            case (Interval(l1, h1), Interval(l2, h2)) =>
              Interval(getMinVal(Set(evaluateTimes(l1, l2), evaluateTimes(l1, h2), evaluateTimes(h1, l2), evaluateTimes(h1, h2))),
                getMaxVal(Set(evaluateTimes(l1, l2), evaluateTimes(l1, h2), evaluateTimes(h1, l2), evaluateTimes(h1, h2))))
          }

        case _ => throw new Exception("undefined")
      }
    }

    expr match {
      case Variable(id) => deltaFunc.deltaFuncMap(id)
      case Plus(_, _) | Minus(_, _) | Times(_, _) => computeIntervalForArithmeticExpr(expr)
      case IntLit(i) => Interval(Integer(i), Integer(i))
      case _ => throw new Exception("undefined")
    }
  }

  def evaluatePlus(i1: IntervalDomain, i2: IntervalDomain): IntervalDomain = {

    i1 match {
      case PositiveInfinity =>
        i2 match {
          case NegativeInfinity => throw new Exception("undefined")

          case _ => PositiveInfinity
        }
      case NegativeInfinity =>
        i2 match {
          case PositiveInfinity => throw new Exception("undefined")
          case _ => NegativeInfinity
        }
      case Integer(i) =>
        i2 match {
          case PositiveInfinity => PositiveInfinity
          case NegativeInfinity => NegativeInfinity
          case Integer(j) => Integer(i + j)
        }

    }

  }

  def evaluateMinus(i1: IntervalDomain, i2: IntervalDomain): IntervalDomain = {

    i1 match {
      case PositiveInfinity =>
        i2 match {
          case PositiveInfinity => throw new Exception("undefined")
          case _ => PositiveInfinity
        }
      case NegativeInfinity =>
        i2 match {
          case NegativeInfinity => throw new Exception("undefined")
          case _ => NegativeInfinity
        }
      case Integer(i) =>
        i2 match {
          case PositiveInfinity => NegativeInfinity
          case NegativeInfinity => PositiveInfinity
          case Integer(j) => Integer(i - j)
        }
    }
  }

  def evaluateTimes(i1: IntervalDomain, i2: IntervalDomain): IntervalDomain = {

    i1 match {
      case PositiveInfinity =>
        i2 match {
          case PositiveInfinity => PositiveInfinity
          case NegativeInfinity => NegativeInfinity
          case Integer(j) => if (j > 0) PositiveInfinity else if (j < 0) NegativeInfinity else throw new Exception("undefined")
        }
      case NegativeInfinity =>
        i2 match {
          case PositiveInfinity => NegativeInfinity
          case NegativeInfinity => PositiveInfinity
          case Integer(j) => if (j > 0) NegativeInfinity else if (j < 0) PositiveInfinity else throw new Exception("undefined")
        }
      case Integer(i) =>
        i2 match {
          case PositiveInfinity => if (i > 0) PositiveInfinity else if (i < 0) NegativeInfinity else throw new Exception("undefined")
          case NegativeInfinity => if (i > 0) NegativeInfinity else if (i < 0) PositiveInfinity else throw new Exception("undefined")
          case Integer(j) => Integer(i * j)
        }
    }
  }

  //for Asserts
  def evaluateDeltaPrime(deltaFunc: DeltaFunc, assertStmt: Tree): DeltaFunc = {

    val involvedVar = allVariablesOf(assertStmt).head
    val deltaPrimeFunMap = deltaFunc.deltaFuncMap + (Identifier(involvedVar) -> computeOptimalInterval(assertStmt, involvedVar, deltaFunc))
    DeltaFunc(deltaPrimeFunMap)
  }

  //for Asserts
  def computeOptimalInterval(tree: Trees.Tree, variable: String, deltaFunc: DeltaFunc): DeltaFuncDomain = ???
  //computes the intersection of two intervals for Asserts evaluation
  def computeIntersection(assertInterval: Interval, existingInterval: DeltaFuncDomain): DeltaFuncDomain = {

    existingInterval match {
      case Bottom => assertInterval
      case Interval(l, h) =>
        val intersectedInterval = Interval(
          if (isSmaller(assertInterval.lowerBound, l)) l else assertInterval.lowerBound,
          if (isSmaller(assertInterval.upperBound, h)) assertInterval.upperBound else h)

        if (isSmaller(intersectedInterval.lowerBound, intersectedInterval.upperBound) ||
          isEqual(intersectedInterval.lowerBound, intersectedInterval.upperBound))
          intersectedInterval
        else Bottom
    }
  }

  //merges the intervals based on the interval analysis lattice
  def mergeIntervals(intervals: Set[DeltaFuncDomain]): DeltaFuncDomain = ???
  def getMaxVal(setOfValues: Set[IntervalDomain]): IntervalDomain = {

    if (setOfValues.contains(PositiveInfinity))
      PositiveInfinity
    else if (setOfValues.contains(NegativeInfinity) && setOfValues.size == 1) NegativeInfinity
    else {
      Integer(setOfValues.filter(x => x != PositiveInfinity && x != NegativeInfinity).map(y => y.asInstanceOf[Integer].i).max)
    }
  }

  def getMinVal(setOfValues: Set[IntervalDomain]): IntervalDomain = {

    if (setOfValues.contains(NegativeInfinity))
      NegativeInfinity
    else if (setOfValues.contains(PositiveInfinity) && setOfValues.size == 1) PositiveInfinity
    else {
      Integer(setOfValues.filter(x => x != PositiveInfinity && x != NegativeInfinity).map(y => y.asInstanceOf[Integer].i).min)
    }
  }

  def widening(setOfKs: Set[IntervalDomain], i1: DeltaFuncDomain, i2: DeltaFuncDomain): DeltaFuncDomain = ???

  def isSmaller(i1: IntervalDomain, i2: IntervalDomain): Boolean = {
    (i1, i2) match {
      case (NegativeInfinity, Integer(_)) | (NegativeInfinity, PositiveInfinity) | (Integer(_), PositiveInfinity) => true
      case (Integer(i), Integer(j)) => i < j
      case _ => false

    }
  }

  def isEqual(i1: IntervalDomain, i2: IntervalDomain): Boolean = {
    (i1, i2) match {
      case (PositiveInfinity, PositiveInfinity) | (NegativeInfinity, NegativeInfinity) => true
      case (Integer(i), Integer(j)) => i == j
      case _ => false
    }
  }
}



