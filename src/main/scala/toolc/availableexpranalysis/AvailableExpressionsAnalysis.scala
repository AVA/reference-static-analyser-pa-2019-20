package toolc.availableexpranalysis

import toolc.ast.Trees
import toolc.ast.Trees.{Variable, _}
import toolc.cfg.{CFG, CFGNode}

object AvailableExpressionsAnalysis {

  /*
   Extracts the set of expressions from the set of CFG nodes
   */
  def createSetOfExpressions(setOfBlocks: List[CFGNode]): Set[ExprTree] = {

    setOfBlocks.map(cfgNode => extractExpressionsFromBlock(cfgNode.tree)).reduce((x, y) => x.union(y))
  }

  /*
   Extracts the valid expressions from the the input expression
   */
  def extractExpressionsFromBlock(cTree: Trees.Tree): Set[ExprTree] = {

    cTree match {

      case Assign(_, expr) =>
        if (isSimpleArithmeticAndContainsVars(expr))
          Set(expr)
        else Set.empty

      case Println(expr) =>
        if (isSimpleArithmeticAndContainsVars(expr))
          Set(expr)
        else Set.empty

      // for expressions in return statement (since they are saved like this as a CFG Node)
      case Plus(_, _) | Minus(_, _) | Times(_, _) | Div(_, _) =>
        if (isSimpleArithmeticAndContainsVars(cTree.asInstanceOf[ExprTree]))
          Set(cTree.asInstanceOf[ExprTree])
        else Set.empty


      case LessThan(lhs: ExprTree, rhs: ExprTree) =>
        Set(lhs).union(Set(rhs)).filter(x => isSimpleArithmeticAndContainsVars(x))

      case Equals(lhs: ExprTree, rhs: ExprTree) =>
        Set(lhs).union(Set(rhs)).filter(x => isSimpleArithmeticAndContainsVars(x))

      case And(lhs: ExprTree, rhs: ExprTree) =>
        extractExpressionsFromBlock(lhs).union(extractExpressionsFromBlock(rhs))

      case Or(lhs: ExprTree, rhs: ExprTree) =>
        extractExpressionsFromBlock(lhs).union(extractExpressionsFromBlock(rhs))

      case Not(expr: ExprTree) =>
        extractExpressionsFromBlock(expr)

      //method arguments
      case _ => Set.empty

    }
  }

  /*
   If an expression is of type Plus, Minus, Times or Div and has at least one variable, returns true
   */
  def isSimpleArithmeticAndContainsVars(expr: ExprTree): Boolean = {

    expr match {
      case Plus(x, y) =>
        x.isInstanceOf[Variable] || y.isInstanceOf[Variable]

      case Minus(x, y) =>
        x.isInstanceOf[Variable] || y.isInstanceOf[Variable]

      case Times(x, y) =>
        x.isInstanceOf[Variable] || y.isInstanceOf[Variable]

      case Div(x, y) =>
        x.isInstanceOf[Variable] || y.isInstanceOf[Variable]

      case _ => false
    }
  }

  def generateTransferFunctionsForBlocks(cfg: CFG): Map[String, Set[TransferFunction]] = ???

  def genKillB(cfgNode: CFGNode, setOfExprs: Set[ExprTree]): Set[ExprTree] = ???

  def genGenB(cfgNode: CFGNode, setOfExprs: Set[ExprTree]): Set[ExprTree] = ???
}

