package toolc.availableexpranalysis

import toolc.ast.Trees.ExprTree
import toolc.cfg.CFG

//f_i = (x_i /kill) union (gen)
case class TransferFunction(id: String, kill: Set[ExprTree], gen: Set[ExprTree]) {}

trait SystemOfEquations {

  /*
   Each equation is an intersect between a set of transferFunctions : Map[String,Set[TransferFunction]]
   The first element of the map is the id of the block and the second is its transfer functions
   */
  def solveSystemOfEquations(setOfEquations: Map[String, Set[TransferFunction]], cfg: CFG): Map[String, Set[ExprTree]] = {

    var previousRoundSolution: Map[String, Set[ExprTree]] = Map.empty
    var currentRoundSolution: Map[String, Set[ExprTree]] = Map.empty

    def updateSolutions(cond: => Boolean, block: => Unit): Unit = {
      block
      if (cond) {
        updateSolutions(cond, block)
      }
    }

    def computeAndUpdateSolutions(): Unit = ???

    //A_exp
    val exprTreeSet = AvailableExpressionsAnalysis.createSetOfExpressions(cfg.setOfBlocks)

    //initializing the first round of solutions to A_exp
    previousRoundSolution = cfg.setOfBlocks.map(node => (node.id, exprTreeSet)).toMap

    currentRoundSolution = previousRoundSolution
    updateSolutions((!previousRoundSolution.equals(currentRoundSolution)), computeAndUpdateSolutions)

    currentRoundSolution
  }

  def toString(fixPoint: Map[String, Set[ExprTree]]): Any = {

    fixPoint.map { case (k, v) => "x_" + k + " = " + v }.mkString("\n")
  }
}


