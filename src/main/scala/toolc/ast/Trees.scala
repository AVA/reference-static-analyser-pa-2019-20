package toolc
package ast

import utils._
import analyzer.Symbols._
import analyzer.Types._

object Trees {
  sealed trait Tree extends Positioned

  // Identifiers represent names in Tool. When a unique symbol gets attached to them,
  // they become unique
  case class Identifier(value: String) extends Tree with Symbolic[Symbol] with Typed {
    override def getType: Type = getSymbol match {
      case cs: ClassSymbol =>
        TClass(cs)

      case ms: MethodSymbol =>
        sys.error("Requesting type of a method identifier.")

      case ms: MainSymbol =>
        sys.error("Requesting type of main object")

      case vs: VariableSymbol =>
        vs.getType
    }
    override def toString = value
  }

  // Definitions
  sealed trait DefTree extends Tree
  case class Program(main: MainObject, classes: List[ClassDecl])
    extends DefTree
  case class MainObject(id: Identifier, stats: List[StatTree])
    extends DefTree with Symbolic[MainSymbol]
  case class ClassDecl(id: Identifier, parent: Option[Identifier], vars: List[VarDecl], methods: List[MethodDecl])
    extends DefTree with Symbolic[ClassSymbol]
  case class VarDecl(tpe: TypeTree, id: Identifier)
    extends DefTree with Symbolic[VariableSymbol]
  case class MethodDecl(id: Identifier,
                        args: List[Formal],
                        retType: TypeTree,
                        vars: List[VarDecl],
                        stats: List[StatTree],
                        retExpr: ExprTree)
    extends DefTree with Symbolic[MethodSymbol]
  sealed case class Formal(tpe: TypeTree, id: Identifier)
    extends DefTree with Symbolic[VariableSymbol]

  // Types
  sealed trait TypeTree extends Tree with Typed
  case class IntArrayType() extends TypeTree {
    override def getType = TIntArray
  }
  case class IntType() extends TypeTree {
    override def getType = TInt
  }
  case class BooleanType() extends TypeTree {
    override def getType = TBoolean
  }
  case class StringType() extends TypeTree {
    override def getType = TString
  }
  case class ClassType(id: Identifier) extends TypeTree {
    override def getType = id.getType
  }

  // Statements
  sealed trait StatTree extends Tree
  case class Block(stats: List[StatTree]) extends StatTree
  case class If(expr: ExprTree, thn: StatTree, els: Option[StatTree]) extends StatTree
  case class While(expr: ExprTree, stat: StatTree) extends StatTree
  case class Println(expr: ExprTree) extends StatTree
  case class Assign(id: Identifier, expr: ExprTree) extends StatTree
  case class ArrayAssign(id: Identifier, index: ExprTree, expr: ExprTree) extends StatTree
  case class DoExpr(e: ExprTree) extends StatTree

  // Expressions
  sealed trait ExprTree extends Tree with Typed

  // Boolean operators
  case class And(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  case class Or(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  case class Not(expr: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  case class Assert(condition: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  // Arithmetic operators (Plus works on any combination of Int/String)
  case class Plus(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    def getType = (lhs.getType, rhs.getType) match {
      case (TInt, TInt) => TInt
      case ((TString | TInt), (TString | TInt)) => TString
      case _ => TError
    }
  }
  case class Minus(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TInt
  }
  case class Times(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TInt
  }
  case class Div(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TInt
  }
  case class LessThan(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  // Equality
  case class Equals(lhs: ExprTree, rhs: ExprTree) extends ExprTree {
    val getType = TBoolean
  }
  // Array expressions
  case class ArrayRead(arr: ExprTree, index: ExprTree) extends ExprTree {
    val getType = TInt
  }
  case class ArrayLength(arr: ExprTree) extends ExprTree {
    val getType = TInt
  }
  case class NewIntArray(size: ExprTree) extends ExprTree {
    val getType = TIntArray
  }
  // Object-oriented expressions
  case class This() extends ExprTree with Symbolic[ClassSymbol] {
    def getType = TClass(getSymbol)
  }
  case class MethodCall(obj: ExprTree, meth: Identifier, args: List[ExprTree]) extends ExprTree {
    def getType = {
      obj.getType match {
        case TClass(os) =>
          os.lookupMethod(meth.value).map(_.getType).getOrElse(TError)
        case _ =>
          TError
      }
    }
  }
  case class New(tpe: Identifier) extends ExprTree {
    def getType = tpe.getType match {
      case t@TClass(_) => t
      case other => TError
    }
  }
  // Literals
  case class IntLit(value: Int) extends ExprTree {
    val getType = TInt
  }
  case class StringLit(value: String) extends ExprTree {
    val getType = TString
  }
  case class True() extends ExprTree {
    val getType = TBoolean
  }
  case class False() extends ExprTree {
    val getType = TBoolean
  }
  // Variables
  case class Variable(id: Identifier) extends ExprTree {
    def getType = id.getType
  }

  /*
   * extracts all the variables of an expression, needed for kill and gen functions in the available expressions analysis
   * also needed for delta Prime Function computation in the interval analysis
   */
  def allVariablesOf(expr: Tree): Set[String] = {

    expr match {

      case Plus(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Minus(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Div(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Times(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case And(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Or(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Not(x) => allVariablesOf(x)

      case Assign(x, y) => allVariablesOf(y)

      case LessThan(x, y) => allVariablesOf(x) ++ allVariablesOf(y)

      case Variable(xid) => Set(xid.toString)

      case _ => Set.empty
    }
  }

  //extracts constants in an expression, needed for widening in the interval analysis
  def allConstantsOf(expr: Tree): Set[IntLit] = {
    expr match {

      case Assign(id, e) => allConstantsOf(e)

      case Plus(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case Minus(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case Div(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case Times(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case And(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case Or(x, y) => allConstantsOf(x) ++ allConstantsOf(y)

      case Not(x) => allConstantsOf(x)

      case LessThan(x, y) =>
        (x, y) match {
          case (IntLit(v), _) => Set(IntLit(v + 1))
          case (_, IntLit(v)) => Set(IntLit(v - 1))
          case _ => throw new Exception("undefined")
        }

      case IntLit(xid) => Set(IntLit(xid))

      case _ => Set.empty
    }
  }

  def isConditional(expr: Tree): Boolean = {
    expr match {
      case And(_, _) | Or(_, _) | Not(_) | LessThan(_, _) | Equals(_, _) => true
      case _ => false
    }
  }

  def simplifyCondition(expr: ExprTree): ExprTree = {
    expr match {
      case LessThan(lhs, rhs) => LessThan(simplifyCondition(lhs), simplifyCondition(rhs))
      case Variable(_) => expr
      case IntLit(i) => expr
      case Plus(_, _) | Minus(_, _) | Times(_, _) => IntLit(evaluateExpr(expr))
      case _ => expr
    }
  }

  def evaluateExpr(expr: Trees.ExprTree): Int = {
    expr match {
      case Plus(lhs, rhs) => evaluateExpr(lhs) + evaluateExpr(rhs)
      case Minus(lhs, rhs) => evaluateExpr(lhs) - evaluateExpr(rhs)
      case Times(lhs, rhs) => evaluateExpr(lhs) * evaluateExpr(rhs)
      case IntLit(i) => i
      case _ => throw new Exception("undefined")
    }
  }

  def containsDivision(expr: Tree): Boolean = {
    expr match {
      case LessThan(lhs, rhs) => containsDivision(lhs) & containsDivision(rhs)
      case Assign(lhs, rhs) => containsDivision(rhs)
      case Minus(lhs, rhs) => containsDivision(lhs) & containsDivision(rhs)
      case Times(lhs, rhs) => containsDivision(lhs) & containsDivision(rhs)
      case Plus(lhs, rhs) => containsDivision(lhs) & containsDivision(rhs)
      case Div(_, _) => true
      case Formal(_, _) | Println(_) | Assert(_) | Variable(_) | IntLit(_) | _ => false
    }
  }
}
