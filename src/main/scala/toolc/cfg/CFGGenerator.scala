package toolc.cfg

import toolc.ast.Trees._
import toolc.utils.Context

trait CFGGenerator {

  //navigates through the the program which is the AST for the input method and generates the CFG
  def generateCFG(v: Program, ctx: Context): CFG = {

    var setOfBlocks: List[CFGNode] = List()
    var initialBlocks: List[CFGNode] = List()
    var controlFlowRelation: List[(CFGNode, CFGNode)] = List()
    var previousStats: Set[CFGNode] = Set()

    def addBlock(b: CFGNode): Unit = {
      setOfBlocks = b :: setOfBlocks
    }

    def addRelation(t1: CFGNode, t2: CFGNode): Unit = {
      controlFlowRelation = (t1, t2) :: controlFlowRelation
    }

    //injects assert expressions in case of interval analysis
    def injectExpr(cfgNode: CFGNode, expr: Tree): Unit = ???

    def handleStatement(stmt: Tree): Unit = {

      stmt match {

        case Assign(_, _) =>

          //adds the assignment statement to the set of blocks
          val cfgNode = CFGNode(stmt, IdFactory.create)
          addBlock(cfgNode)

          //creates edges from the previous statements to the current one
          previousStats.map(addRelation(_, cfgNode))

          //makes the current statement the new previous statement
          previousStats = Set(cfgNode)

        case Println(_) =>

          //adds the print statement to the set of blocks
          val cfgNode = CFGNode(stmt, IdFactory.create)
          addBlock(cfgNode)

          //creates edges from the previous statements to the current one
          previousStats.map(addRelation(_, cfgNode))

          //makes the current statement the new previous statement
          previousStats = Set(cfgNode)


        // statements inside the "if-else" and "while" are enclosed by blocks
        case Block(exprTree) =>
          exprTree.map(handleStatement)

        case If(expr, thn, els) =>

          if (ctx.doIntervalAnalysis) {

            //adds the if condition to the set of blocks and simplifies the condition
            val cfgNode = CFGNode(simplifyCondition(expr), IdFactory.create)
            addBlock(cfgNode)

            //creates edges from the previous statements to the if condition block
            previousStats.map(addRelation(_, cfgNode))

            //handles the then part of the statement
            previousStats = Set(cfgNode)

            //injects an Assert block after the if condition block
            injectExpr(cfgNode, Assert(simplifyCondition(expr)))

            handleStatement(thn)

            //keeps the previous statements for later
            val tempIfPrevStats = previousStats

            //handles the else part of the statement if exists
            if (els.nonEmpty) {
              previousStats = Set(cfgNode)

              //injects the negation of the Assert condition in the beginning of the else block
              injectExpr(cfgNode, Assert(Not(simplifyCondition(expr))))

              handleStatement(els.get)

              previousStats ++= tempIfPrevStats
            } else {
              previousStats += cfgNode
            }
          } else if (ctx.doAvailableExprsAnalysis) {
            //TODO: Implement!
          }

        case While(expr, stat) =>

          if (ctx.doIntervalAnalysis) {

            //TODO: Implement!

          } else if (ctx.doAvailableExprsAnalysis) {

            //adds while if condition to the set of blocks
            val cfgNode = CFGNode(expr, IdFactory.create)
            addBlock(cfgNode)

            //creates edges from the previous statements to while condition block
            previousStats.map(addRelation(_, cfgNode))

            previousStats = Set(cfgNode)

            handleStatement(stat)

            //creates an edge from the last statement of the while loop to the while condition block
            previousStats.map(addRelation(_, cfgNode))

            //adjusts the previous stat to the while condition block
            previousStats = Set(cfgNode)
          }

        case _ =>
      }
    }

    //adding method arguments to the initial block as they are "read([variable_name])" type of statements
    val handleInitialBlocks = (t: Tree) => {
      val cfgInitialNode = CFGNode(t, IdFactory.create)
      addBlock(cfgInitialNode)
      initialBlocks = cfgInitialNode :: initialBlocks
      previousStats += cfgInitialNode
    }
    v.classes.head.methods.head.args.map(handleInitialBlocks)

    //navigate through the method statements
    v.classes.head.methods.head.stats.map(handleStatement)

    //the return statement is added to the set of blocks
    val returnExprNode = CFGNode(v.classes.head.methods.head.retExpr, IdFactory.create)
    addBlock(returnExprNode)

    //the edges from the previous statements to the final block (which is the return statement) are created
    previousStats.map(addRelation(_, returnExprNode))

    CFG(setOfBlocks, initialBlocks, controlFlowRelation)
  }
}



