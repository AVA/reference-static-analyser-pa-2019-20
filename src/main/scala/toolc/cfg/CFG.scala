package toolc.cfg

import toolc.ast.Trees._

//defining a CFG
case class CFG(setOfBlocks: List[CFGNode], initialBlocks: List[CFGNode], controlFlowRelation: List[(CFGNode, CFGNode)]) {

  //prints all the edges of the Control Flow Graph
  override def toString: String = {

    "\nNode Id -> Node Id <------> Node Content -> Node Content\n" +
      controlFlowRelation.map { case (v1, v2) => s"${v1.id}  -> ${v2.id}  <------> ${v1.tree}  -> ${v2.tree}" }.mkString("\n")
  }

  //outputs the previous nodes of a node in the CFG
  def getPreviousNodesOf(id: String): List[CFGNode] = {
    controlFlowRelation.filter { case (cn1, cn2) => cn2.id == id }.map(_._1)
  }

  //returns true if the condition blocks are simple enough for our interval analysis
  def areConditionsSimple(): Boolean = {

    //conditional statements
    val conditionals = this.setOfBlocks.filter(cfgNode => isConditional(cfgNode.tree))

    if (conditionals.isEmpty) true
    //if we have a condition statement other than LessThan
    else if (conditionals.exists(cfgNode => !cfgNode.tree.isInstanceOf[LessThan])) false
    else {
      //computes the maximum number of vars occurring in the conditional statement
      val maxNumVars = conditionals.map(x => allVariablesOf(x.tree).size).max
      maxNumVars == 1
    }
  }

  //returns true if any of the cfg blocks contains division which we cannot handler in our interval analysis
  def hasDivision: Boolean = {
    this.setOfBlocks.map(cfgNode => containsDivision(cfgNode.tree)).reduce((a, b) => a | b)
  }

  //outputs all the constants in the program for computing the set of Ks in widening
  def getConstants: Set[IntLit] = {
    this.setOfBlocks.map(x => allConstantsOf(x.tree)).toSet.reduce((c1, c2) => c1.union(c2))
  }
}

/*
 Each tree node needs a unique id since we may have similar statements in different parts of the method
 */
case class CFGNode(tree: Tree, id: String) {
}

/*
 Creates unique id
 */
object IdFactory {

  private var counter = 0

  def create: String = {

    counter += 1
    counter.toString
  }
}
