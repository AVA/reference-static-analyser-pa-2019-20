program AvailableExprSlideExample {}

class AbsoluteValue {

  def codeSnippet (a : Int , b : Int ) : Int = {

    var x : Int;
    var y : Int;

    x = a + b;
    y = a * b;

    while (a + b < y) {
      a = a + 1;
      x = a + b;
    }

    return x;
  }
}