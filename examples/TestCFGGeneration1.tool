program TestCFGGeneration1 {}

class AbsoluteValue {

  def absoluteX ( x : Int ) : Int = {

    var y : Int;
    y =	0;

    while (x < 0) {
      x = x + 1;
      y = y + 1;
    }

    return y;
  }
}