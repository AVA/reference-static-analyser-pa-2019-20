program IntervalAnalysisSlideExample {}

 class CountTo42 {

     def codeSnippet (i : Int) : Int = {

       i = 0;
       while (i < 43) {
         i = i + 1;
       }
       return i;
     }
   }